#include <ros/ros.h>
#include <boost/thread.hpp>

#include <imu_msgs/MPU9150.h>

#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "MotionSensor.h"

#define delay_ms(a) usleep(a*1000)

int main(int argc, char** argv)
{
	ros::init(argc, argv, "mpu9150_driver");
	ros::NodeHandle n;

	ros::Publisher pub_data = n.advertise<imu_msgs::MPU9150>("mpu9150_data", 10);

	imu_msgs::MPU9150 imu;

	ros::Rate r(40);

	ms_open();
	while(ros::ok()){
		ms_update();
		imu.euler.x = ypr[ROLL];
		imu.euler.y = ypr[PITCH];
		imu.euler.z = ypr[YAW];

		imu.gyro.x = gyro[0];
		imu.gyro.y = gyro[1];
		imu.gyro.z = gyro[2];

		imu.accel.x = accel[2];
		imu.accel.y = accel[1];
		imu.accel.z = accel[0];

		imu.compass.x = compass[0];
		imu.compass.y = compass[1];
		imu.compass.z = compass[2];

		imu.temprature = temp;
		//printf("yaw = %2.1f\tpitch = %2.1f\troll = %2.1f\ttemperature = %2.1f\tcompass = %2.1f, %2.1f, %2.1f\n",
		// ypr[YAW], ypr[PITCH],
		// ypr[ROLL],temp,compass[0],compass[1],compass[2]);
		//delay_ms(5);

		pub_data.publish(imu);

		//r.sleep();
	}

	return 0;
}
